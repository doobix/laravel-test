<!DOCTYPE html>
<html lang="es">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <title>¡Zapatitos!</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}">
  </head>

  <body>

    <div class="container">

      @yield('content')

      <div class="footer">
        <p class="text-center">
          zapatitos SL © 2013 · +34 658 548 584 · <a href="mailto:help@zapatitos.es">help@zapatitos.es</a>
        </p>
      </div>

    </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

  <!-- Latest compiled and minified JavaScript -->
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <!--  Main js  -->
  <script src="{{ URL::asset('assets/js/main.js') }}"></script>
  <!-- RRSSB js -->
  <script src="{{ URL::asset('assets/js/rrssb.js') }}"></script>

  </body>

</html>
