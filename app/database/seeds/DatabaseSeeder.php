<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ZapatitoTableSeeder');
		$this->command->info('Zapatito table seeded!');
	}

}


class ZapatitoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('zapatitos')->delete();

        Zapatito::create(array(
					'tipo' 		=> 'Adidas performance F5 y TRX TF J',
					'precio' 	=> '59.90'
				));
    }

}
