# paravel test

## Requirements

Git, Apache, PHP >= 5.3.7, MCrypt PHP Extension, MySQL

## Install

### Install composer

http://getcomposer.org/doc/00-intro.md

### Clone this repository

```
$ git clone git@bitbucket.org:doobix/laravel-test.git
```

### Setup composer

```
$ composer install
```

## Config

### Config MySQL connection

Set your connection parameters in ```app/config/database.php``` around line 55.

### Run migrations

```
$ php artisan migrate
```

### Seed the database

```
$ php artisan db:seed
```

## Start

```
$ php artisan serve
```
and then go to http://localhost:8000/prueba.

> Nota: si quieres que arranque en http://localhost/prueba has de arrancar el servidor así ```$ php artisan serve --port=80``` (seguramente necesitarás permisos de administrador).
